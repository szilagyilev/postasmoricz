//
//  UIView+CornerRadius.swift
//  PostasMoricz
//
//  Created by Levente Szilágyi on 2020. 03. 08..
//  Copyright © 2020. szilagyilev. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = true
        }
    }
}
