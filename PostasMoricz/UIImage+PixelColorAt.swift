//
//  UIImage+PixelData.swift
//  PostasMoricz
//
//  Created by Levente Szilágyi on 2020. 03. 08..
//  Copyright © 2020. szilagyilev. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    func pixelColorAt (x: Int, y: Int) -> UIColor? {
        guard x >= 0 && x < Int(size.width) && y >= 0 && y < Int(size.height),
            let cgImage = cgImage,
            let provider = cgImage.dataProvider,
            let providerData = provider.data,
            let data = CFDataGetBytePtr(providerData) else {
            return nil
        }

        let numberOfComponents = 4
        let pixelData = ((Int(size.width) * y) + x) * numberOfComponents

        let r = CGFloat(data[pixelData]) / 255.0
        let g = CGFloat(data[pixelData + 1]) / 255.0
        let b = CGFloat(data[pixelData + 2]) / 255.0
        let a = CGFloat(data[pixelData + 3]) / 255.0

        return UIColor(red: r, green: g, blue: b, alpha: a)
    }
    
    func isEmptyPixelAt (x: Int, y: Int) -> Bool {
        guard x >= 0 && x < Int(size.width) && y >= 0 && y < Int(size.height),
            let providerData = cgImage?.dataProvider?.data,
            let data = CFDataGetBytePtr(providerData) else {
            return false
        }
        
        let pixelData = ((Int(size.width) * y) + x) * 4

        return CGFloat(data[pixelData]) + CGFloat(data[pixelData + 1]) + CGFloat(data[pixelData + 2]) + CGFloat(data[pixelData + 3])  == 0
    }
}
