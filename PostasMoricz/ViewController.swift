//
//  ViewController.swift
//  PostasMoricz
//
//  Created by Levente Szilágyi on 2020. 03. 08..
//  Copyright © 2020. szilagyilev. All rights reserved.
//

import UIKit

enum MoveDirection {
    case north
    case northEast
    case east
    case southEast
    case south
    case southWest
    case west
    case northWest
    case calculating
    case none
}

class ViewController: UIViewController {
    
    // MARK: - Public Variables
    public var fillPercentage: CGFloat = 0.0
    
    // MARK: - IBOutlets
    @IBOutlet weak var playerDragView: UIImageView?
    @IBOutlet weak var playerContainerView: UIView?
    @IBOutlet weak var playerView: UIImageView?
    @IBOutlet weak var imageView: UIImageView?
    @IBOutlet weak var infoContainer: UIVisualEffectView?
    @IBOutlet weak var infoLabel: UILabel?
    @IBOutlet weak var percentageLabel: UILabel?
    @IBOutlet weak var returnButton: UIButton?
    @IBOutlet weak var resetButton: UIButton?
    
    @IBOutlet weak var doggoContainerView: UIView?
    @IBOutlet weak var doggoView: UIImageView?
    
    // MARK: - Private Variables
    private var initialCenter = CGPoint()
    private var lastPoint = CGPoint()
    private var isMoriczAtStart = false
    private var currentMoveDirection: MoveDirection = .none
    
    // Check every nth row and nth column when counting the pixels of the drawing
    // v1 CPU Usage - 1: ~70%, 2: ~25%
    // v2 CPU Usage - 1: ~50%, 2: ~21%
    private let countingStrideSize = 1
    
    // MARK: - Animation Images
    let northRunImages = (1...2).map { UIImage(named: "N\($0)")! }
    let southRunImages = (1...2).map { UIImage(named: "S\($0)")! }
    let eastRunImages = (1...2).map { UIImage(named: "E\($0)")! }
    let westRunImages = (1...2).map { UIImage(named: "W\($0)")! }
    let calculatingImages = (1...2).map { UIImage(named: "C\($0)")! }
    let idleImages = [UIImage(named: "Idle")!]

    let doggoEastRunImages = (1...2).map { UIImage(named: "DE\($0)")! }
    let doggoWestRunImages = (1...2).map { UIImage(named: "DW\($0)")! }
    let doggoIdleImages = (1...2).map { UIImage(named: "DI\($0)")! }
    
    
    // MARK: - Public Functions
    @IBAction func returnButtonTapped(_ sender: Any) {
        resetMoricz()
    }
    
    @IBAction func resetButtonTapped(_ sender: Any) {
        resetAll()
    }
    
    @IBAction func panMoricz(_ gestureRecognizer : UIPanGestureRecognizer) {
        guard let piece = playerContainerView else {
            return
        }
        // Get the changes in the X and Y directions relative to
        // the superview's coordinate space.
        let translation = gestureRecognizer.translation(in: piece.superview)
        if gestureRecognizer.state == .began {
            // Save the view's original position.
            initialCenter = piece.center
            hideInfoView()
            isMoriczAtStart = false
        }
        // Update the position for the .began, .changed, and .ended states
        if gestureRecognizer.state != .ended {
            // Add the X and Y translation to the view's original position.
            let newCenter = CGPoint(x: initialCenter.x + translation.x, y: initialCenter.y + translation.y)
            
            let dir = moveDirection(oldPosition: piece.center, newPosition: newCenter)
            
            if currentMoveDirection != dir {
                animateMovement(inDirection: dir)
                print("animate")
                currentMoveDirection = dir
            }
            piece.center = newCenter
            let drawCenter = CGPoint(x: newCenter.x, y: newCenter.y + 64)
            drawLines(fromPoint: lastPoint, toPoint: drawCenter)
            lastPoint = drawCenter
        }
        else {
            // On cancellation, return the piece to its original location.
            guard let playerView = playerView else {
                return
            }
            moveDoggoToMoricz()
            
            playerView.stopAnimating()
            animateMovement(inDirection: .calculating)
            
            let targetUiImage = imageView?.image
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                guard let self = self else {
                    return
                }

                var percentage = self.doLongCalculation(targetUiImage)
                percentage = min(percentage, 100)
                
                DispatchQueue.main.async { [weak self] in
                    self?.percentageLabel?.text = "\(String(format: "%.2f", percentage))%"
                    self?.showInfoView()
                    self?.animateMovement(inDirection: .none)
              }
            }
        }
    }
    
    // MARK: - Private Functions
    // MARK: View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        resetMoricz()
    }
    
    // MARK: - Drawing
    private func moveDoggoToMoricz() {
        guard let doggoView = doggoView,
              let playerContainerView = playerContainerView,
              let doggoContainerView = doggoContainerView else {
            return
        }
        
        let newCenter = CGPoint(x: doggoView.center.x+(playerContainerView.center.x - doggoView.center.x)*0.75,
                                y: doggoView.center.y+(playerContainerView.center.y - doggoView.center.y)*0.75)
        
        if newCenter.y <= playerContainerView.center.y {
            view.bringSubviewToFront(playerContainerView)
        } else {
            view.bringSubviewToFront(doggoContainerView)
        }
        
        if let infoContainer = infoContainer {
            view.bringSubviewToFront(infoContainer)
        }
        
        UIView.animate(withDuration: 1.0, animations: { [weak self] in
            
            self?.animateDoggo(inDirection: self?.getDoggoDirection() ?? .none)
            
            
            doggoView.center = newCenter
        },completion:
            { [weak self] finished in
                self?.animateDoggo(inDirection: .none)
        })
    }
    
    private func getDoggoDirection() -> MoveDirection {
        guard let doggoView = doggoView,
              let playerContainerView = playerContainerView else {
                return .none
        }
        
        if playerContainerView.center.x >= doggoView.center.x {
            return .east
        }else {
            return .west
        }
    }
    
    private func getFillPercentageOfImage(_ targetUIImage: UIImage?) -> CGFloat {
        guard let targetImage = targetUIImage else {
                  return 0
        }
        
        let imageWidth = Int(targetImage.size.width)
        let imageHeight = Int(targetImage.size.height)
        
        var numberOfWhiteColoredPixels = 0
        
        for xC in stride(from: 0, to: imageWidth, by: countingStrideSize) {
            for yC in stride(from: 0, to: imageHeight, by: countingStrideSize) {
                if !targetImage.isEmptyPixelAt(x: xC, y: yC) {
                    numberOfWhiteColoredPixels += 1 * countingStrideSize * countingStrideSize
                }
            }
        }
        
        return CGFloat(numberOfWhiteColoredPixels) / CGFloat(imageWidth*imageHeight)
    }
    
    private func drawLines(fromPoint:CGPoint,toPoint:CGPoint) {
        UIGraphicsBeginImageContext(self.view.frame.size)
        imageView?.image?.draw(in: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        let context = UIGraphicsGetCurrentContext()
        
        context?.move(to: CGPoint(x: fromPoint.x, y: fromPoint.y))
        context?.addLine(to: CGPoint(x: toPoint.x, y: toPoint.y))
        
        context?.setBlendMode(CGBlendMode.normal)
        context?.setLineCap(CGLineCap.round)
        context?.setLineWidth(36.0)
        context?.setStrokeColor(UIColor(red: 0.66, green: 0.75, blue: 0.66, alpha: 1.0).cgColor)
        
        context?.strokePath()
        
        imageView?.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
    
    // MARK: - Layout Management
    private func hideInfoView() {
        UIView.animate(withDuration: 0.5, animations: { [weak self] in
            self?.infoContainer?.alpha = 0.0
        })
    }
    
    private func showInfoView() {
        UIView.animate(withDuration: 0.5, animations: { [weak self] in
            self?.infoContainer?.alpha = 1.0
        })
        
        updateReturnButton()
        updateResetButton()
    }

    private func updateReturnButton() {
        guard let returnButton = returnButton else {
            return
        }
        returnButton.isHidden = isMoriczAtStart
    }
    
    private func updateResetButton(fromReset: Bool = false) {
        guard let resetButton = resetButton else {
            return
        }
        resetButton.isHidden = !isMoriczAtStart || fromReset
    }
    
    // MARK: - State Management
    private func resetMoricz() {
        UIView.animate(withDuration: 0.5, animations: { [weak self] in
            guard let self = self else {
                return
            }
            self.playerContainerView?.center = self.view.center
            self.animateMovement(inDirection: .south)
        }, completion: { [weak self] (finished: Bool) in
            guard let self = self else {
                return
            }
            
            self.playerView?.stopAnimating()
        })
        
        initialCenter = CGPoint(x: view.center.x, y: view.center.y + 64)
        lastPoint = initialCenter
        isMoriczAtStart = true
        
        showInfoView()
    }
    
    func resetAll() {
        imageView?.image = nil
        fillPercentage = 0
        percentageLabel?.text = "\(String(format: "%.2f", fillPercentage))%"
        updateResetButton(fromReset: true)
    }
    
    // MARK: - Character Animation
    private func doLongCalculation(_ newImage: UIImage?) -> CGFloat {
        guard let uiImage = newImage else {
            return 0.0
        }
        
      return getFillPercentageOfImage(uiImage)*100
    }
    
    private func animateDoggo(inDirection direction: MoveDirection) {
        guard let doggoView = doggoView else {
            return
        }
        
        switch direction {
        case .north, .southWest, .northWest, .west :
            doggoView.animationImages = doggoWestRunImages
        case .northEast, .southEast, .south, .east:
            doggoView.animationImages = doggoEastRunImages
        case .calculating, .none:
            doggoView.animationImages = doggoIdleImages
        }
        
        if !doggoView.isAnimating {
            print("start doggo animation")
            doggoView.animationDuration = 0.2
            doggoView.animationRepeatCount = 0
            doggoView.startAnimating()
        }
    }
    
    private func animateMovement(inDirection direction: MoveDirection) {
        guard let playerView = playerView else {
            return
        }

        switch direction {
        case .north, .northEast, .northWest:
            playerView.animationImages = northRunImages
        case .south, .southEast, .southWest:
            playerView.animationImages = southRunImages
        case .west:
            playerView.animationImages = westRunImages
        case .east:
            playerView.animationImages = eastRunImages
        case .calculating:
            playerView.animationImages = calculatingImages
        case .none:
            playerView.animationImages = idleImages
        }
        
        if !playerView.isAnimating {
            print("start animation")
            playerView.animationDuration = 0.2
            playerView.animationRepeatCount = 0
            playerView.startAnimating()
        }
    }
    
    private func moveDirection(oldPosition: CGPoint, newPosition: CGPoint) -> MoveDirection {
        
        let deadZone: CGFloat = 5.0
        
        if newPosition.y < oldPosition.y - deadZone {
            // Moves northbound
            if newPosition.x < oldPosition.x - deadZone  {
                // Moves west and northbound
                print("NW")
                return .northWest
            } else if newPosition.x > oldPosition.x + deadZone {
                // Moves east and northbound
                print("NE")
                return .northEast
            }
        } else if newPosition.y > oldPosition.y + deadZone {
            // Moves southbound
            if newPosition.x < oldPosition.x - deadZone {
                // Moves west and southbound
                print("SW")
                return .southWest
            } else if newPosition.x > oldPosition.x + deadZone {
                // Moves east and southhbound
                print("SE")
                return .southEast
            }
        } else if oldPosition.x - deadZone ... oldPosition.x + deadZone ~= newPosition.x {
            // Moves vertically
            if newPosition.y < oldPosition.y {
                // Moves northbound
                print("N")
                return .north
            } else if newPosition.y > oldPosition.y {
                // Moves southhbound
                print("S")
                return .south
            }
        } else if oldPosition.y - deadZone ... oldPosition.y + deadZone ~= newPosition.y {
            // Moves horizontally
            if newPosition.x < oldPosition.x {
                // Moves westbound
                print("W")
                return .west
            } else if newPosition.x > oldPosition.x {
                // Moves eastbound
                print("E")
                return .east
            }
        }
        print("-")
        return currentMoveDirection
    }
}
